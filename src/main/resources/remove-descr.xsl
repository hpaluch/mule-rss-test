<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- default - vse zkopirovat na vystup -->
<xsl:template match="@*|node()|comment()">
  <xsl:copy>
     <xsl:apply-templates select="@*|node()|comment()"/>
  </xsl:copy>
</xsl:template>

<!-- odstranit element description -->
<xsl:template match="description"/>

</xsl:stylesheet>
