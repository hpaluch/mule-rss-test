package org.bitbucket.hpaluch.mule_rss_test;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.mule.api.MuleContext;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.endpoint.ImmutableEndpoint;
import org.mule.api.lifecycle.InitialisationException;
import org.mule.api.transformer.DataType;
import org.mule.api.transformer.Transformer;
import org.mule.api.transformer.TransformerException;

public class MyCollectionToStringTransformer implements Transformer {

	@Override
	public MuleEvent process(MuleEvent ev) throws MuleException {
//		System.out.println("YEAH1 "+ev);
//		System.err.println("YEAH1 "+ev);
		Object payload = ev.getMessage().getPayload();
		StringBuffer out = new StringBuffer();
		if (payload instanceof Collection){
			 Collection c = (Collection)payload;
			 for(Iterator i=c.iterator();i.hasNext();){
				 Object o = i.next();
				 if (out.length()>0){
					 out.append("\r\n");
				 }
				 out.append(o.toString());
			 }
			 
		} else {
			out.append(payload.toString());
		}
		
		ev.getMessage().setPayload(out.toString());
		return ev;
	}

	@Override
	public void initialise() throws InitialisationException {

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setName(String arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setMuleContext(MuleContext arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setEndpoint(ImmutableEndpoint arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getEncoding() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImmutableEndpoint getEndpoint() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getMimeType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Class<?> getReturnClass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DataType<?> getReturnDataType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DataType<?>> getSourceDataTypes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Class<?>> getSourceTypes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAcceptNull() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isIgnoreBadInput() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSourceDataTypeSupported(DataType<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSourceTypeSupported(Class<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setReturnClass(Class<?> arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setReturnDataType(DataType<?> arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public Object transform(Object arg0) throws TransformerException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object transform(Object arg0, String arg1)
			throws TransformerException {
		// TODO Auto-generated method stub
		return null;
	}

}
