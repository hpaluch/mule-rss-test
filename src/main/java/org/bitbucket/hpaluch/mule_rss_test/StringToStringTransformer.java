package org.bitbucket.hpaluch.mule_rss_test;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.mule.api.transformer.DataType;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractTransformer;
import org.mule.transformer.types.DataTypeFactory;
import org.mule.util.IOUtils;
/**
 * This trivial transformer is used to avoid Exception:
 * Payload was invalidated calling setPayload and the message is not collection anymore.
 * 
 * This occures when CollectionOfMessage payload is changed to  Message payload followed by component call.
 * 
 * @author hpaluch
 *
 */
public class StringToStringTransformer extends AbstractTransformer {

	public StringToStringTransformer(){
		registerSourceType(DataType.STRING_DATA_TYPE);
		setReturnDataType(DataType.STRING_DATA_TYPE);
	}
	
	
	@Override
	protected Object doTransform(Object src, String outputEncoding)
			throws TransformerException {
		return src.toString();
	}
	
    protected String createStringFromByteArray(byte[] bytes, String outputEncoding) throws TransformerException
    {
        try
        {
            return new String(bytes, outputEncoding);
        }
        catch (UnsupportedEncodingException uee)
        {
            throw new TransformerException(this, uee);
        }
    }

    protected String createStringFromInputStream(InputStream input)
    {
        try
        {
            return IOUtils.toString(input);
        }
        finally
        {
            IOUtils.closeQuietly(input);
        }
    } 
}
